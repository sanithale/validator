package com.xzitt.validator;

/**
 * Created By Sanit Thale
 * * 11/21/2018 3:45 PM
 **/
public class Validator {

    public static void main(String[] args) throws Exception {
        String input = "Y'QD HDQBB9W HDQUUGF DDQ88AA";
        String[] mutipleInput = {"Y'HDQBB9W", "Y'HDQUUGF", "Y'DDQ88AA"};
        Validator validator = new Validator();
        validator.validateInputStream(input);
        for (String mInput : mutipleInput) {
            validator.validateInputStream(mInput);
        }
    }

    private void validateInputStream(String input) throws Exception {
        if (input.startsWith("Y'QD")) {
            input = input.replace("Y'QD","").trim();
            String[] values = input.split(" ");
            if(values.length > 32) throw new Exception("More than 32 entries");
            for (String value : values) {
                if(value.length() != 7) throw new Exception("Invalid Entry");
            }
        } else if (input.startsWith("Y'")){
            String value = input.split("Y'")[1];
            if(value.length() != 7) throw new Exception("Invalid Entry");
        }
    }
}
